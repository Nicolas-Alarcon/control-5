#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "estudiantes.h"


float desvStd(estudiante curso[]){ //Se define la funcion desvStd
	int i;
	float MediaArit = 0.0, varianza = 0.0, DesvEst;
	for (i = 0; i < 40; i++) //Se inserta un contador hasta un maximo de 40 estudiantes
	{
		MediaArit = curso[i].prom + MediaArit; //Se define la primera parte de la media aritmetica
	}
	MediaArit = (MediaArit)/2; //Se define la media aritmetica final
	for (i = 0; i < 40; i++) //Se inserta un contador hasta un maximo de 40 estudiantes
	{
		varianza = pow(curso[i].prom - MediaArit, 2) + varianza; //Se eleva con la opcion pow por medio de la libreria math.h
	}
	DesvEst = sqrt(varianza/39); //Se obtiene la raiz cuadrada con la opcion sqrt por medio de la libreria math.h
	printf("La desviacion estandar final es: %.2f\n", DesvEst); //Muestra la desvicacion estandar obtenida
	return 0.0;
}

float menor(float prom[]) //Se define la funcion menor
{
	int i;
	float PromMenor = prom[0];
	for (i = 1; i < 40; i++) //Se inserta un contador hasta un maximo de 40 estudiantes
	{
		if (PromMenor > prom[i]) //Mientras que esta sentencia no se cumpla no se guarda el promedio menor
		{
			PromMenor = prom[i];
		}
	}
	printf("El promedio mas bajo es: %.2f\n", PromMenor); //Muestra el promedio mas bajo obtenido
	return 0.0;
}

float mayor(float prom[])
{
	int i;
	float PromMayor = prom[0];
	for (i = 1; i < 40; i++) //Se inserta un contador hasta un maximo de 40 estudiantes
	{
		if (PromMayor < prom[i]) //Mientras que esta sentencia no se cumpla no se guarda el promedio menor
		{
			PromMayor = prom[i];
		} 
	}
	printf("El promedio mas alto es: %.2f\n", PromMayor); //Muestra el promedio mas alto obtenido
	return 0.0;
}

void registroCurso(estudiante curso[]){ //Se define la funcion registroCurso
	int i = 0;
	float PromProyFinal, PromContFinal;
	for (i = 0; i < 40; i++){ //Se inserta un contador hasta un maximo de 40 estudiantes
		printf("\n Estudiante : %s %s %s\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM); //Muestra en pantalla el nombre de los estudiantes
		printf("Ingrese las notas de todos los proyectos separados por espacios : \n");
		scanf("%f %f %f", &curso[i].asig_1.proy1, &curso[i].asig_1.proy2, &curso[i].asig_1.proy3); //Se guardan las variables de los proyectos
		printf("Ingrese las notas de los controles separados por espacios : \n");
		scanf("%f %f %f %f %f %f", &curso[i].asig_1.cont1, &curso[i].asig_1.cont2, &curso[i].asig_1.cont3, &curso[i].asig_1.cont4, &curso[i].asig_1.cont5, &curso[i].asig_1.cont6); //Se guardan las variables de los controles
		PromProyFinal = (curso[i].asig_1.proy1 * 0.20) + (curso[i].asig_1.proy2 * 0.20) + (curso[i].asig_1.proy3 * 0.30); //Se aplica el porcentaje a cada proyecto respectivamente como la que se nos aplicara a nosotros
		PromContFinal = (curso[i].asig_1.cont1 * 0.05) + (curso[i].asig_1.cont2 * 0.05) + (curso[i].asig_1.cont3 * 0.05) + (curso[i].asig_1.cont4 * 0.05) + (curso[i].asig_1.cont5 * 0.05) + (curso[i].asig_1.cont6 * 0.05); //Se aplica el porcentaje a cada control respectivamente como la que se nos aplicara a nosotros
		curso[i].prom = (PromProyFinal + PromContFinal); //Se obtiene el promedio final para cada estudiante
	}
}

void clasificarEstudiantes(char path[], estudiante curso[]){
	printf("TEST"); 
}


void metricasEstudiantes(estudiante curso[]){ //Se define la funcion metricasEstudiantes
	float desvStd(estudiante curso[]); //Muestra la desvicacion estandar obtenida
	float menor(float prom[]); //Muestra el promedio mas bajo obtenido
	float mayor(float prom[]); //Muestra el promedio mas alto obtenido
}





void menu(estudiante curso[]){ //Se define la funcion menu
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //Muestra en forma de lista los nombres de los estudiantes
					break;

			case 2: registroCurso(curso);// Realiza el ingreso de todas las notas obtenidas por el estudiante
					break;

			case 3: metricasEstudiantes(curso); //Muestra métricas de disperción, como mejor promedio; más bajo; desviación estándar.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;

            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[40]; //Se define como un maximo de 40 estudiantes
	menu(curso); //Se llama a la funcion menu
	return EXIT_SUCCESS; 
}